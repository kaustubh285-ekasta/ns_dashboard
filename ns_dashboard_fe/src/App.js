import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Topbar from "./components/Topbar";
import SignIn from "./pages/Login";

import Home from "./pages/Home";

function App() {
  return (
    <div className="app">
      <Router>
        <Switch>
          <Route path="/login">
            <SignIn />
          </Route>
          <Route path="/">
            <Topbar />
            <Home />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
