import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  logout__button: {
    color: "white",
    borderColor: "white",
    outlineWidth: 0,
    outline: 0,
  },
}));

export default function Topbar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          ></IconButton>
          <Typography variant="h6" className={classes.title}>
            NS Dashboard
          </Typography>
          <Button color="default" className={classes.logout__button}>
            <a
              href="/user/dashboard/logout"
              style={{ color: "white", textDecoration: "none" }}
            >
              Logout
            </a>
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}
