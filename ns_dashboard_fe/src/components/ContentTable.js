import React, { useEffect, useState } from "react";

import "../css/ContentTable.css";

function ContentTable({ apiRoute, usersRoute }) {
  const [fetchedData, setFetchedData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await fetch(`http://52.66.210.170${apiRoute}`);
      console.log("Data fetched");
      const body = await result.json();
      console.log(body);
      setFetchedData(body);
    };
    fetchData();
  }, [apiRoute]);
  return (
    <div className="contentTable w-100">
      {usersRoute && <h4> User Details</h4>}

      {!usersRoute && <h4> Order Details</h4>}

      <table className="table contentTable__table">
        {usersRoute && (
          <thead>
            <tr className="contentTable__tableHeader">
              <th>Id</th>
              <th>Email</th>
              <th>Name</th>
              <th>Orders</th>
            </tr>
          </thead>
        )}
        {!usersRoute && (
          <thead>
            <tr className="contentTable__tableHeader">
              <th></th>
              <th>Kirana</th>
              <th>Marketplace</th>
              <th>Total</th>
            </tr>
          </thead>
        )}

        <tbody>
          {usersRoute &&
            fetchedData.map((oneLine) => (
              <tr key={oneLine.id}>
                <td>{oneLine.id}</td>
                <td>{oneLine.email}</td>
                <td>{oneLine.name}</td>
                <td>{oneLine.number_of_orders}</td>
              </tr>
            ))}
          {!usersRoute &&
            fetchedData.map((oneLine, id) => (
              <tr key={id}>
                <td className="contentTable__rowHeader">
                  <b>{oneLine.header}</b>
                </td>
                <td>{oneLine.kirana}</td>
                <td>{oneLine.market}</td>
                <td>{oneLine.total}</td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
}

export default ContentTable;
