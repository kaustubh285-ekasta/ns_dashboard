import { Button } from "@material-ui/core";
import React, { useState } from "react";
import ContentTable from "../components/ContentTable";
import "../css/Home.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { SkipNext } from "@material-ui/icons";

function Home() {
  const [startDate, setStartDate] = useState(new Date(2020, 9, 1));

  const [endDate, setEndDate] = useState(new Date());

  const [apiRoute, setApiRoute] = useState("/api/users");
  const [usersRoute, setUsersRoute] = useState(true);

  const handleRouteChange = (route, value) => {
    setApiRoute(route);
    setUsersRoute(value);
  };

  return (
    <div className="container home">
      <div className="home__filter">
        <div className="home__filterButtons">
          <Button
            variant="outlined"
            onClick={() => handleRouteChange("/api/users", true)}
          >
            Users Detail
          </Button>
          <Button
            variant="outlined"
            onClick={() => handleRouteChange("/api/orders", false)}
          >
            Order Detail
          </Button>
        </div>
        <div className="home__filterDates">
          <div className="home__filterDate">
            <h6>Dates:</h6>
            <DatePicker
              selected={startDate}
              onChange={(date) => setStartDate(date)}
            />
          </div>

          <div className="home__filterDate">
            <h6>To:</h6>
            <DatePicker
              selected={endDate}
              onChange={(date) => setEndDate(date)}
            />
          </div>
        </div>
      </div>
      <ContentTable apiRoute={apiRoute} usersRoute={usersRoute} />
    </div>
  );
}

export default Home;
