"""dashboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView
from ns_dashboard_be.views import user_data , orders_data, user_logout
from django.contrib.auth.decorators import login_required


admin.site.site_header = "NS-Dashboard"
admin.site.site_title = "NS-Dashboard"
admin.site.index_title = "NS-Dashboard"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',login_required(TemplateView.as_view(template_name='index.html'))),
    path('api/users', user_data),
    path('api/orders', orders_data),
    path('user/dashboard/logout', user_logout)
]
