from django.http import request
from django.shortcuts import render
from django.http import Http404, HttpResponse, JsonResponse
import psycopg2
import psycopg2.extras 
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect

# Create your views here.


def user_logout(request):
    logout(request)
    response = redirect('/')
    return response
    



def user_data(request, *args, **kwargs):
    try:
        connection = psycopg2.connect(user= "ekasta", host= "db.ekastaplatform.com", database= "Ekasta", password= "beacon5791",port= 5432,)
        cursor = connection.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
        # Print PostgreSQL Connection properties        
        cursor.execute('SELECT users.uid,users.id,users.name,users.email, count(orders.id) as number_of_orders from users left join orders on (users.id = orders."userId") where orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True group by users.id')
        record = cursor.fetchall()

        columns = ('uid','id',  'name','email', 'number_of_orders')

        results = []
        for row in record:
            results.append(dict(zip(columns, row)))
        
        cursor.close()
        connection.close()
        
        
        userData = JsonResponse(results, status=200, safe=False)
        
        userData['Access-Control-Allow-Origin'] = '*'
        userData["Access-Control-Allow-Headers"] = "Origin, X-Requested-With, Content-Type,Accept"

        return  userData

    except (Exception, psycopg2.Error) as error :
        return HttpResponse({error}, status=400)
    return JsonResponse({"message":"Unkown error occured"}, status=200, safe=False)
    

def orders_data(request, *args, **kwargs):
    try:
        connection = psycopg2.connect(user= "ekasta", host= "db.ekastaplatform.com", database= "Ekasta", password= "beacon5791",port= 5432,)
        cursor = connection.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
        # Print PostgreSQL Connection properties
        # ! CHange Datesto current date from midnight
        query1=' select  count(id) as count_total, SUM(bill_amount) as sum_total, (select count(orders.id) as count_market from orders where "storeId"=13 and orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True) , (select count(orders.id) as count_kirana from orders where "storeId"!=13 and orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True) , (select SUM(bill_amount) as sum_market from orders where "storeId"=13 and orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True),(select SUM(bill_amount)  as sum_kirana from orders where "storeId"!=13 and  orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True) from orders where orders."createdAt">=timestamp \'2020-10-01 00:00:00\'and orders.paid=True'
        
        # ! CHange Datesto current date from midnight
        # -> store id !13 & no of orders >1
        repeat_kirana='(select count(orders.id) as count_market, SUM(orders.bill_amount) from orders where "storeId"!=13 and orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True and orders."userId" in (select x.uid from (select "userId" as uid ,count(order_id) as ord from orders where orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True GROUP BY "userId") as x where x.ord>1))'
        # ! CHange Datesto current date from midnight
        # -> store id 13 & no of orders >1
        repeat_market='(select count(orders.id) as count_market, SUM(orders.bill_amount) from orders where "storeId"=13 and orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True and orders."userId" in (select x.uid from (select "userId" as uid ,count(order_id) as ord from orders where orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True GROUP BY "userId") as x where x.ord>1))'
        # ! CHange Datesto current date from midnight
        # -> no of orders >1
        repeat_total='(select count(orders.id) as count_market, SUM(orders.bill_amount) from orders where orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True and orders."userId" in (select x.uid from (select "userId" as uid ,count(order_id) as ord from orders where orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True GROUP BY "userId") as x where x.ord>1))'
        # -> store id 13 & no of orders =1
        new_kirana='(select count(orders.id) as count_market, SUM(orders.bill_amount) from orders where "storeId"!=13 and orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True and orders."userId" in (select x.uid from (select "userId" as uid ,count(order_id) as ord from orders where orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True GROUP BY "userId") as x where x.ord=1))'
        # -> store id !13 & no of orders =1
        new_market='(select count(orders.id) as count_market, SUM(orders.bill_amount) from orders where "storeId"=13 and orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True and orders."userId" in (select x.uid from (select "userId" as uid ,count(order_id) as ord from orders where orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True GROUP BY "userId") as x where x.ord=1))'
        # -> no of orders =1
        new_total='(select count(orders.id) as count_market, SUM(orders.bill_amount) from orders where orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True and orders."userId" in (select x.uid from (select "userId" as uid ,count(order_id) as ord from orders where orders."createdAt">=timestamp \'2020-10-01 00:00:00\' and orders.paid=True GROUP BY "userId") as x where x.ord=1))'
        
        all_queries = [query1, repeat_kirana, repeat_market, repeat_total, new_kirana, new_market, new_total]
        
        all_records = []
        for query in all_queries:
            cursor.execute(query)
            record = cursor.fetchall()
            for values in record:
                for x in values:
                    all_records.append(x)

        
        columns = ('header','kirana','marketplace','total')

        results = []
        for row in record:
            results.append(dict(zip(columns, row)))
        
        cursor.close()
        connection.close()
        
        order_results = [
            {
                "header":"No of Orders",
                "kirana":all_records[3],
                "market":all_records[2],
                "total":all_records[0],
            },
            {
                "header":"Value of Orders",
                "kirana":("%.2f" % all_records[5]),
                "market":("%.2f" % all_records[4]),
                "total":("%.2f" % all_records[1]),
            },
            {
                "header":"Repeat Customer Count",
                "kirana":all_records[6],
                "market":all_records[8],
                "total":all_records[10],
            },
            {
                "header":"Repeat Customer Value",
                "kirana":("%.2f" % all_records[7]),
                "market":("%.2f" % all_records[9]),
                "total":("%.2f" % all_records[11]),
            },
            {
                "header":"New Customer Count",
                "kirana":all_records[12],
                "market":all_records[14],
                "total":all_records[16],
            },
            {
                "header":"New Customer Value",
                "kirana":("%.2f" % all_records[13]),
                "market":("%.2f" % all_records[15]),
                "total":("%.2f" % all_records[17]),
            }
        ]
        
        orderData = JsonResponse(order_results, status=200, safe=False)
        
        orderData['Access-Control-Allow-Origin'] = '*'
        orderData["Access-Control-Allow-Headers"] = "Origin, X-Requested-With, Content-Type,Accept"

        return  orderData

    except (Exception, psycopg2.Error) as error :
        return HttpResponse({error}, status=400)
    return JsonResponse({"message":"Unkown error occured"}, status=200, safe=False)