from django.apps import AppConfig


class NsDashboardBeConfig(AppConfig):
    name = 'ns_dashboard_be'
